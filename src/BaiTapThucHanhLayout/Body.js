import React, { Component } from 'react'

export default class Body extends Component {
  render() {
    return (
    <div>
  <header className="py-5">
    <div className="container px-lg-5">
      <div className="p-4 p-lg-5 bg-light rounded-3 text-left">
        <div className="m-4 m-lg-5">
          <h1 className="display-4 fw-bold">A warm welcome!</h1>
          <p className="fs-4">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex maxime velit eligendi ullam aliquid suscipit?</p>
          <a className="btn btn-primary btn-lg" href="#!">Call to action!</a>
        </div>
      </div>
    </div>
  </header>
 

 
</div>

    )
  }
}
