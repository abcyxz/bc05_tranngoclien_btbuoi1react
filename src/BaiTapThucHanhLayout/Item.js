import React, { Component } from 'react'

export default class Item extends Component {
  render() {
    return (
      <div>
      <div className="container px-lg-5">
  <div className="row">
    <div className=" col-sm">
      <div className="card">
        <div className="card-body">
          <img src='...'></img>
          <h5 className="card-title">Special title treatment</h5>
          <p className="card-text">
            With supporting text below as a natural lead-in to
            additional content.
          </p>
          <a href="#" className="btn btn-primary">
            Go somewhere
          </a>
        </div>
      </div>
    </div>
    <div className=" col-sm">
      <div className="card">
        <div className="card-body">
        <img src='...'></img>

          <h5 className="card-title">Special title treatment</h5>
          <p className="card-text">
            With supporting text below as a natural lead-in to
            additional content.
          </p>
          <a href="#" className="btn btn-primary">
            Go somewhere
          </a>
        </div>
      </div>
    </div>
    <div className=" col-sm">
      <div className="card">
        
        <div className="card-body">
        <img src='...'></img>

          <h5 className="card-title">Special title treatment</h5>
          <p className="card-text">
            With supporting text below as a natural lead-in to
            additional content.
          </p>
          <a href="#" className="btn btn-primary">
            Go somewhere
          </a>
        </div>
      </div>
    </div>
    <div className=" col-sm">
      <div className="card">
        <div className="card-body">
        <img src='...'></img>

          <h5 className="card-title">Special title treatment</h5>
          <p className="card-text">
            With supporting text below as a natural lead-in to
            additional content.
          </p>
          <a href="#" className="btn btn-primary">
            Go somewhere
          </a>
        </div>
      </div>
    </div>
  </div>
</div>

      </div>
    )
  }
}
